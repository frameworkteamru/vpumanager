<?php

namespace FrameWorkTeam\Vpumanager\Lib;

class Telegram
{
    protected $token;

    protected $chat_id;

    protected $message;

    protected $url;

    public function __construct($setting)
    {
        $this->token   = $setting['token'];
        $this->chat_id = $setting['chatId'];
        $this->message = $setting['name'] . ' | ';
        $this->url = $setting['url'];
    }

    public function sendReport($report)
    {
        foreach ($report as $key=>$value) {
            $this->message .= $key .' : ' . $value .' | ';
        }

        $parameters = [
            'chat_id' => $this->chat_id,
            'text'    => $this->message,
        ];

        $url = 'https://api.telegram.org/bot' . $this->token . '/sendMessage?' . http_build_query($parameters);

        file_get_contents($url);
    }

    public function sendError($report)
    {
        $this->message .= 'Ошибок: ' .$report['failed']. ' | ';
        $this->message .= 'Ссылка: ' .$this->url. ' | ';

        $parameters = [
            'chat_id' => $this->chat_id,
            'text'    => $this->message,
        ];
        $url = 'https://api.telegram.org/bot' . $this->token . '/sendMessage?' . http_build_query($parameters);

        file_get_contents($url);
    }
}
