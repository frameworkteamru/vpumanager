<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVpumReportTable extends Migration
{
    public function up()
    {
        Schema::create('vpum__report', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->boolean('status')->default(0);
            $table->integer('succeeded')->default(0);
            $table->integer('skipped')->default(0);
            $table->integer('incomplete')->default(0);
            $table->integer('failed')->default(0);
            $table->integer('total')->default(0);
            $table->integer('percentSucceeded')->default(0);
            $table->integer('percentSkipped')->default(0);
            $table->integer('percentIncomplete')->default(0);
            $table->integer('percentFailed')->default(0);
            $table->binary('report')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('vpum__report');
    }
}
