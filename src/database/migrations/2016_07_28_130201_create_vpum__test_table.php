<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVpumTestTable extends Migration
{

    public function up()
    {
        Schema::create('vpum__test', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->unique();
            $table->string('path', 255);
            $table->boolean('web_status')->default(1);
            $table->boolean('cron_status')->default(1);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('vpum__test');
    }
}