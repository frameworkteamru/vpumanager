<p class="h3">Last Project PHPUnit test:</p>

<span class="label label-success"> Success: {{ $lastTest['summary_of_testing']['succeeded'] }}</span>

@if ($lastTest['summary_of_testing']['skipped'])
    <span class="label label-info">Skipped: {{ $lastTest['summary_of_testing']['skipped']}}</span>
@endif

@if ($lastTest['summary_of_testing']['incomplete'])
    <span class="label label-warning">Incomplete: {{ $lastTest['summary_of_testing']['incomplete'] }}</span>
@endif

@if ($lastTest['summary_of_testing']['failed'])
    <span class="label label-danger">Failed: {{$lastTest['summary_of_testing']['failed'] }}</span>
@endif

<span class="label label-primary">Total: {{$lastTest['summary_of_testing']['total'] }}</span>

