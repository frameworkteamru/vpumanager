@if (isset($lastTest['tests']))

    @foreach($lastTest['tests'] as $key => $test)

        @if (!empty($test['suites']) && isset($test['stats']))
            <div class="panel {{ !$test['stats']['tests']['failed'] ? 'panel-success' : 'panel-danger' }}">
                <div class="panel-heading">
                    Name: {{$key}}
                </div>
                <div class="panel-body">
                    @if ($test['suites'][$key]['status']=='succeeded')
                        <div class="alert alert-default" role="alert">
                            The test is successful for {{$test['suites'][$key]['time']}}
                        </div>
                        @foreach($test['suites'][$key]['tests'] as $method_test)

                            @if($method_test['output'])

                                <div class="alert alert-info" role="alert">
                                    <button class="btn btn-info btn-xs" type="button" data-toggle="collapse"
                                            data-target="#{{ $method_test['name'] }}" aria-expanded="false"
                                            aria-controls="collapseExample">
                                        Output {{ $method_test['name'] }}
                                    </button>

                                    <div class="collapse" id="{{ $method_test['name'] }}">
                                        <div class="well">
                                            <pre>{{ $method_test['output']}}</pre>
                                        </div>
                                    </div>
                                </div>

                            @endif

                        @endforeach

                    @else

                        @foreach($test['suites'][$key]['tests'] as $method_test)

                            @if ($method_test['status'] == 'failed')

                                <div class="alert alert-danger" role="alert">
                                    <p><strong>{{ $method_test['name'] }}</strong></p>
                                    <pre>{{ $method_test['message'] }}</pre>

                                    @if($method_test['output'])

                                        <button class="btn btn-danger btn-xs" type="button" data-toggle="collapse"
                                                data-target="#{{ $method_test['name'] }}" aria-expanded="false"
                                                aria-controls="collapseExample">
                                            {{ $method_test['name'] }} output
                                        </button>

                                        <div class="collapse" id="{{ $method_test['name'] }}">
                                            <div class="well">
                                                <pre>{{ $method_test['output']}}</pre>
                                            </div>
                                        </div>

                                    @endif

                                </div>

                            @endif

                        @endforeach

                    @endif
                </div>
                <div class="panel-footer">
                    <p class="h4">
                        @if ($test['stats']['tests']['succeeded'])
                        <span class="label label-success"> Success: {{ $test['stats']['tests']['succeeded'] }}</span>
                        @endif

                        @if ($test['stats']['tests']['skipped'])
                            <span class="label label-info">Skipped: {{ $test['stats']['tests']['skipped'] }}</span>
                        @endif

                        @if ($test['stats']['tests']['incomplete'])
                            <span class="label label-warning">Incomplete: {{ $test['stats']['tests']['incomplete'] }}</span>
                        @endif

                        @if ($test['stats']['tests']['failed'])
                            <span class="label label-danger">Failed: {{ $test['stats']['tests']['failed'] }}</span>
                        @endif

                        <span class="label label-primary">Total: {{ $test['stats']['tests']['total'] }}</span>
                    </p>
                </div>
            </div>
        @endif

    @endforeach
@endif