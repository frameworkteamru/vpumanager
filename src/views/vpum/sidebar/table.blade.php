@if (isset($tests) && ($tests->count()!= 0))
    <table class="table table-hover table-condensed text-center test-table">
        <thead>
            <td>ID</td>
            <td class="text-left">Name</td>
            <td>WEB</td>
            <td>CRON</td>
        </thead>
        <tbody>
            @foreach($tests as $test)
                @include('vpum.sidebar.item', ['test' => $test])
            @endforeach
        </tbody>
    </table>
@else
    <a href="{{route('vpumanager.generate')}}">Generate test list</a>
@endif