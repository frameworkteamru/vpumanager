<tr>
    <td>{{$test->id}}</td>
    <td class="text-left">{{$test->name}}</td>
    <td>
        <a href="#" data-id="{{$test->id}}" data-status="{{$test->web_status}}"
           data-type="web">
            <span class='glyphicon {{$test->web_status==1?"glyphicon-ok":"glyphicon-remove"}}'
                  aria-hidden='true'></span>
        </a>
    </td>
    <td>
        <a href="#" data-id="{{$test->id}}" data-status="{{$test->cron_status}}"
           data-type="cron">
            <span class='glyphicon {{$test->cron_status==1?"glyphicon-ok":"glyphicon-remove"}}'
                  aria-hidden='true'></span>
        </a>
    </td>
</tr>