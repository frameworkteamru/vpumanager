<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">PHPUnit Test list:</div>
            <div class="panel-body">
                @include('vpum.sidebar.table')
            </div>
        </div>

        <a href="{{route('vpumanager.startphpunit')}}" class="btn btn-primary">Start PHPUnit</a>
        <a href="{{route('vpumanager.generate')}}" class="btn btn-info">
            <span class="glyphicon glyphicon-retweet" aria-hidden="true"></span>
        </a>
        <a href="{{route('vpumanager.deletetestlist')}}" class="btn btn-danger">
            <span class="glyphicon glyphicon-fire" aria-hidden="true"></span>
        </a>

    </div>
</div>