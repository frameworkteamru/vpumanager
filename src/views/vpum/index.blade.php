@extends('vpum.layouts.master')

@section('sidebar')
    @include('vpum.sidebar.index')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            @if (isset($lastTest) && is_array($lastTest))
                <div class="panel panel-info">

                    <div class="panel-heading">
                        @include('vpum.test.heading')
                    </div>
        
                    <div class="panel-body">
                        <p class="h4">List of tests:</p>

                        @include('vpum.test.body', ['lastTest' => $lastTest])
                    </div>
                </div>
            @else
                <div class="jumbotron">
                    <h1>PHPUnit never be started !</h1>
                    <p>Follow command :</p>
                    <pre>php artisan vpu:phpunit</pre>
                    <p> or click START!</p>
                    <p><a class="btn btn-primary btn-lg" href="{{route('vpumanager.startphpunit')}}" role="button">Start PHPUnit</a></p>
                </div>
            @endif

        </div>
    </div>
@endsection
