<?php

return [
    'name' =>
        "VPUManager Package v0.1",

    'test_patch' =>
        base_path('tests'),

    'php' => env('VPU_PHP_PATH', ''), ///home/bin/php5.6

    'phpunit_path' => env('VPU_PHPUNIT_PATH', ''), ///home/users/s/srogin/sites/otlichnik/phpunit.phar

    'phpunit' =>
        base_path('phpunit.xml'),

    'telegram_token' =>
        '235722677:AAH8rtRFDXBhkLXsu2gTe1_B6phJs8xoJBM',

    'chatId' =>
        '-179194702'
];