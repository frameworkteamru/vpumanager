<?php

namespace FrameWorkTeam\Vpumanager\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

use FrameWorkTeam\Vpumanager\Lib\VPU;
use FrameWorkTeam\Vpumanager\Lib\Telegram;

use App\Model\Vpum\VpumTest;
use App\Model\Vpum\VpumReport;

class VpumMakeCommand extends Command
{
    protected $signature = 'vpu:phpunit
                        {--web : Web action status}
                        {--telegram : Send message by config}
                        {--error : Send message if error by config}';

    protected $description = 'phpunit test project';

    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();

        $this->filesystem = $filesystem;
    }

    public function handle()
    {
        $pathXml     = config('vpumanager.phpunit');
        $phpPath     = config('vpumanager.php');
        $phpunitPath = config('vpumanager.phpunit_path');
        $action      = 'cron_status';

        if ($this->option('web'))
        {
            $action = 'web_status';
        }

        if ($this->option('telegram') || $this->option('error'))
        {
            $name   = config('vpumanager.name');
            $token  = config('vpumanager.telegram_token');
            $chatId = config('vpumanager.chatId');
            $url    = url('_vpumanager');

            $settingsTelegram = compact('name' , 'token' , 'chatId' , 'url');
        }

        $pathTests = VpumTest::select('name' , 'path')
            ->where($action , true)
            ->get();

        if (!$pathTests->isEmpty())
        {
            $results = self::getPHPUnitTest($pathTests , $pathXml , $phpPath , $phpunitPath);

            $report = $results['summary_of_testing'];

            self::getResultInfo($report);

            self::saveReport($results);

            if ($this->option('telegram'))
            {
                self::sendReport($settingsTelegram , $report);
            }

            if ($this->option('error') && $report['failed'])
            {
                self::sendError($settingsTelegram , $report);
            }
        }
        else
        {
            $this->error('PHPUnit tests not found!');
            $this->line('Follow command "php artisan vpu:generate" and try again !');
        }
    }

    protected function getPHPUnitTest($pathTests , $pathXml , $phpPath , $phpunitPath)
    {
        $vpu = new VPU();

        $summaryOfTesting = array(
            'succeeded'  => 0 ,
            'skipped'    => 0 ,
            'incomplete' => 0 ,
            'failed'     => 0 ,
            'total'      => 0 ,
        );

        $results = array();

        foreach ($pathTests as $pathTest)
        {
            $this->info('Configuring '.$pathTest->name.'...');

            $command = "$phpPath $phpunitPath phpunit -c $pathXml $pathTest->path --stderr";

            $html_errors = ini_get('html_errors');

            ini_set('html_errors' , 0);
            $shellResult = shell_exec($command);
            ini_set('html_errors' , $html_errors);

            $report = $vpu->reportGenerator($shellResult , 'web' , $summaryOfTesting , $pathTest->name);

            $summaryOfTesting = $report['summary_of_testing'];
            unset($report['summary_of_testing']);

            $results['tests'][$pathTest->name] = $report;

            $this->info('Configuring '.$pathTest->name.' finished!');
        }

        $results['summary_of_testing'] = $vpu->getPercentages($summaryOfTesting);

        return $results;
    }

    protected function getResultInfo($summaryOfTesting)
    {
        $this->line('Summary of testing:');
        $this->info('Success:'.$summaryOfTesting['succeeded']);
        $this->line('Skipped:'.$summaryOfTesting['skipped']);
        $this->line('Incomplete:'.$summaryOfTesting['incomplete']);
        $this->error('Failed:'.$summaryOfTesting['failed']);
        $this->info('Total:'.$summaryOfTesting['total']);
    }

    protected function saveReport($fullStat)
    {
        if (is_array($fullStat))
        {
            $report = new VpumReport();

            foreach ($fullStat['summary_of_testing'] as $key => $value)
            {
                $report->$key = $value;
            }

            if ($report->failed == 0)
            {
                $report->status = true;
            }

            $report->name   = config('vpumanager.name');
            $report->report = json_encode($fullStat);

            $report->save();
        }
    }

    protected function sendReport($setting , $report)
    {
        $telegram = new Telegram($setting);
        $telegram->sendReport($report);
    }

    protected function sendError($setting , $report)
    {
        $telegram = new Telegram($setting);
        $telegram->sendError($report);
    }
}