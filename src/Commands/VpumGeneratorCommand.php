<?php

namespace FrameWorkTeam\Vpumanager\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

use App\Model\Vpum\VpumTest;

class VpumGeneratorCommand extends Command
{
    protected $signature = 'vpu:generate';

    protected $description = 'phpunit generate all test project';

    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();

        $this->filesystem = $filesystem;
    }

    public function handle()
    {
        $testsPatch = config('vpumanager.test_patch');

        $testsPath = $this->filesystem->files($testsPatch);

        foreach ($testsPath as $testPath)
        {
            if (str_is('*Test.php' , $testPath))
            {
                $filename = pathinfo($testPath , PATHINFO_FILENAME);

                $path = str_replace(['/' , '\\'] , DIRECTORY_SEPARATOR , $testPath);

                VpumTest::firstOrCreate(['name' => $filename , 'path' => $path]);

                $this->info('Save in the base '.$filename.' !');
            }
        }
    }

}
