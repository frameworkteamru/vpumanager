<?php
namespace FrameWorkTeam\Vpumanager;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Artisan;

class VpumanagerServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $routeConfig = [
            'namespace' => 'FrameWorkTeam\Vpumanager\Controllers',
            'prefix' => '_vpumanager',
            'middleware' => 'web'
        ];

        $this->app['router']->group($routeConfig, function ($router) {

            $router->get('/', [
                'uses' => 'VpumController@index',
                'as' => 'vpumanager.index',
            ]);

            $router->post('/editstatus', [
                'uses' => 'VpumController@editStatus',
                'as' => 'vpumanager.editstatus',
            ]);

            $router->get('/generate', [
                'uses' => 'VpumController@generateTest',
                'as' => 'vpumanager.generate',
            ]);

            $router->get('/startphpunit', [
                'uses' => 'VpumController@startTest',
                'as' => 'vpumanager.startphpunit',
            ]);

            $router->get('/deletetestlist', [
                'uses' => 'VpumController@deleteTestList',
                'as' => 'vpumanager.deletetestlist',
            ]);
        });

        $this->loadViewsFrom(__DIR__ . '/views/vpum/', 'vpum');

        $this->publishes([__DIR__ . '/views/vpum/' => base_path('resources/views/vpum')]);

        $this->publishes([__DIR__ . '/Model/' => base_path('app/Model/Vpum'),]);

        $this->publishes([__DIR__ . '/database/migrations' => base_path('database/migrations'),]);

        $this->publishes([__DIR__ . '/config/vpumanager.php' => config_path('vpumanager.php')]);

        $this->publishes([__DIR__ . '/public/' => public_path() . "/vendor/vpum/"], 'assets');

    }

    public function register()
    {
        $this->registerVpuManager();
        $this->registerVpumGenerator();
    }

    private function registerVpuManager()
    {
        $this->app->singleton('command.fwt.vpum', function ($app) {
            return $app['FrameWorkTeam\Vpumanager\Commands\VpumMakeCommand'];
        });

        $this->commands('command.fwt.vpum');
    }

    private function registerVpumGenerator()
    {
        $this->app->singleton('command.fwt.generator', function ($app) {
            return $app['FrameWorkTeam\Vpumanager\Commands\VpumGeneratorCommand'];
        });

        $this->commands('command.fwt.generator');
    }
}