$(document).ready(function () {

    $('table.test-table a').on('click', function (events) {
        events.preventDefault();
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        var type = $(this).attr('data-type');
        var button = $(this);
        editStatus(id, status, type, button);
    });

    function editStatus(id, status, type, button) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            url: urlEdit,

            type: "POST",

            dataType: 'json',

            data: {'id': id, 'status': Number(status), 'type': type},

            success: function (data) {
                button.attr("data-status", Number(data['status']));
                if (data['status'] == false) {
                    button.find("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
                }
                else {
                    button.find("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
                }
            }
        });
    };
});
