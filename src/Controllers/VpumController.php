<?php
namespace FrameWorkTeam\Vpumanager\Controllers;

use Artisan;

use App\Http\Controllers\Controller;
use App\Model\Vpum\VpumTest;
use App\Model\Vpum\VpumReport;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VpumController extends Controller
{
    public function index()
    {
        $tests    = VpumTest::all();
        $lastTest = VpumReport::orderby('id' , 'desc')->first();

        if ($lastTest)
        {
            $lastTest = json_decode($lastTest->report , true);
        }

        return view('vpum::index' , ['tests' => $tests , 'lastTest' => $lastTest]);
    }

    public function editStatus(Request $request)
    {
        $this->validate($request , [
            'id'     => 'required|integer' ,
            'status' => 'required|boolean' ,
            'type'   => 'required|in:web,cron' ,
        ]);

        $id     = $request['id'];
        $status = !(boolean)$request['status'];
        $test   = VpumTest::find($id);

        if ($request['type'] == 'web')
        {
            $test->web_status = $status;
        }
        elseif ($request['type'] == 'cron')
        {
            $test->cron_status = $status;
        }

        $test->update();

        return response()->json([
            'id'     => $id ,
            'status' => $status ,
        ]);
    }

    public function generateTest()
    {
        DB::table('vpum__test')->truncate();

        Artisan::call('vpu:generate');

        return redirect()->route('vpumanager.index');
    }

    public function startTest()
    {
        Artisan::call('vpu:phpunit' , ['--web' => true ,]);

        return redirect()->route('vpumanager.index');
    }

    public function deleteTestList()
    {
        DB::table('vpum__test')->truncate();

        return redirect()->route('vpumanager.index');
    }
}