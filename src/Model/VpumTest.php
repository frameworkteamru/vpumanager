<?php

namespace App\Model\Vpum;

use Illuminate\Database\Eloquent\Model;

class VpumTest extends Model
{
    protected $table = "vpum__test";

    protected $fillable = [
        'name',
        'path',
        'web_status',
        'cron_status',
    ];

    public $timestamps = true;
}
