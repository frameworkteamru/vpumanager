<?php

namespace App\Model\Vpum;

use Illuminate\Database\Eloquent\Model;

class VpumReport extends Model
{
    protected $table = "vpum__report";

    protected $fillable = [
        'report',
        'status',
        'succeeded',
        'skipped',
        'incomplete',
        'failed',
        'total',
        'percentSucceeded',
        'percentSkipped',
        'percentIncomplete',
        'percentFailed',
        'name'
    ];

    public $timestamps = true;
}
